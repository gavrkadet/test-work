function addNewComment() {                                                  //Создание коммента
    let targetToPaste = $("#comments")                                      //Куда будем вставлять новый комент
    let nodeForCopy = $(".comment, .hidden")                                //Шаблон узла комментария
    let newComment = nodeForCopy[0].cloneNode(true)                    //Копируем шаблон комментария
    $(newComment).removeClass('hidden')
    if ($('#text_new_comment').val() !== "") {
        newComment.childNodes[1].textContent = $('#text_new_comment').val() //Кладем текст комментария из textarea в <p>
        $('#text_new_comment').val("")
        $.ajax({
                url: '/test-work/index.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    txtNewComment: newComment.childNodes[1].textContent,
                },
                success: function (data) {
                    $(newComment.childNodes[5]).attr('id', data)
                    pasteNewElementWithEventListener(data, targetToPaste[0], newComment)
                },
                error: function () {
                    console.log('Error')
                }
            }
        )
    } else {
        alert('Комментарий не может быть пустым!')
    }
}

function viewAllComments() {                    //Отобразить все коменты
    let nodeForCopy = $(".comment, .hidden")
    $.ajax({
            url: '/test-work/index.php',
            type: 'POST',
            dataType: 'json',
            data: {
                getAllComments: true,
            }, success: function (data) {
                $('#text_new_comment').val("")
                let targetPaste = $("#comments")                            //DOM-узел для вставки
                for (let i = 0; i < data.items.length; i++) {
                    let comment = nodeForCopy[0].cloneNode(true)
                    $(comment).removeClass('hidden')
                    //кладем текст коментария в <p id="text_comment"></p>
                    comment.childNodes[1].textContent = data.items[i].text_comment
                    //кладем id в <p id="id_comment"></p>
                    $(comment.childNodes[5]).attr('id', data.items[i].id)
                    pasteNewElementWithEventListener(data.items[i].id, targetPaste, comment)
                    if (data.items[i].hasOwnProperty('children')) {
                        viewChildren(data.items[i].children, comment)
                    }
                }
            },
            error: function () {
                console.log('Error')
            }
        }
    )
}

function viewChildren(children, parent) {                  //Отображение потомков коментов, children - array objects
    for (let i = 0; i < children.length; i++) {
        let comment = parent.cloneNode(true)
        comment.childNodes[1].textContent = children[i].text_comment
        $(comment.childNodes[5]).attr('id', children[i].id)
        while (comment.childNodes.length !== 7) {
            comment.lastChild.remove()
        }
        pasteNewElementWithEventListener(children[i].id, parent, comment)
        if (children[i].hasOwnProperty('children')) {
            viewChildren(children[i].children, comment)
        }
    }
}

function editThisComment(idComment) {                       //Изменение комментария
    $('#send').off('click')
    let oldTxtComment = $('#text_new_comment').val()
    $('#send').on('click', function () {
        let txtComment = $('#text_new_comment').val();
        if (txtComment !== "") {
            $.ajax({
                    url: '/test-work/index.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        editComment: true,
                        txtComment: txtComment,
                        idComment: idComment,
                    },
                    success: function () {
                        $('#id_comment').text($('#text_new_comment').val())
                        document.getElementById(idComment).parentNode.children[0].textContent = txtComment
                    },
                    error: function () {
                        console.log('Error')
                    }
                }
            )
            $('#text_new_comment').val(""), $('#send').off('click')
            $('#send').on('click', () => addNewComment())
        } else {
            alert("Комментарий не может быть пустым!")
            $('#text_new_comment').val(oldTxtComment)
        }
    })
}

function answerComment(idAnswerComment) {                                           //Ответить на комент
    let current = document.getElementById(idAnswerComment)
    console.log(current.parentNode.childNodes)
    if (current.parentNode.childNodes.length !== 17) {                              //вложенность комментариев не больше 10!
        $('#send').off('click')
        let targetToPaste = document.getElementById(idAnswerComment).parentNode     //Куда будем вставлять ответ на комент
        let nodeForCopy = $(".comment, .hidden")                                    //Шаблон узла комментария
        let newComment = nodeForCopy[0].cloneNode(true)                        //Копируем шаблон комментария
        $(newComment).removeClass('hidden')
        newComment.childNodes[1].textContent = $('#text_new_comment').val()
        $('#send').on('click', function () {
            if ($('#text_new_comment').val() !== "") {
                newComment.childNodes[1].textContent = $('#text_new_comment').val() //Передаем текст комментария из textarea в <p>
                $('#text_new_comment').val("")
                $.ajax({
                        url: '/test-work/index.php',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            isAnswer: true,
                            txtComment: newComment.childNodes[1].textContent,
                            idAnswerComment: idAnswerComment,
                        },
                        success: function (data) {
                            pasteNewElementWithEventListener(data, targetToPaste, newComment)
                        },
                        error: function () {
                            console.log('Error')
                        }
                    }
                )
                $('#text_new_comment').val(""), $('#send').off('click')
                $('#send').on('click', () => addNewComment())
            } else {
                alert('Комментарий не может быть пустым!')
            }
        })
    }
    alert('Вложенность комментариев не должна превышать 10!')
}

function pasteNewElementWithEventListener(id, targetToPaste, newComment) {  //data=id, targetToPaste - DOM узел, куда вставляем
    targetToPaste.append(newComment);
    //Вешаем обработчик событий на кнопку "Изменить"
    $(newComment.childNodes[3].childNodes[1]).on("click", function () {
        $("#text_new_comment").val(newComment.childNodes[1].textContent)
        editThisComment(id)
    })
    //вешаем обработчик событий на кнопку "Ответить"
    $(newComment.childNodes[3].childNodes[3]).on("click", function () {
        $('#text_new_comment').val("")
        answerComment(id)
    })
}
