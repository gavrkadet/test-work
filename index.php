<?php

use Evgeny\Test\DB\DB;
use Rundiz\NestedSet\NestedSet;

require_once __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
$config = require __DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.php';

$db = new DB();
$nestedSet = new NestedSet($db->getLink());
$nestedSet->tableName = $config['table_name'];

//Если пришел запрос Post с getAllComments, получаем все записи из таблицы
if (isset($_POST['getAllComments'])) {
    try {
        $options = [];
        $options['unlimited'] = true;
        $listComments = $nestedSet->listComments($options);
        unset($options);

        echo json_encode($listComments);
    } catch (PDOException $e) {
        echo 'Database error: ' . $e->getMessage();
    }
}
//Если пришел запрос Post с textComment, создаем запись в таблице
if (isset($_POST['txtNewComment'])) {
    try {
        $positionNewComment = $nestedSet->getNewPosition(0);
        $db->execute("INSERT INTO {$config['table_name']} (parent_id, text_comment, position) 
                          VALUES (0, '{$_POST['txtNewComment']}', {$positionNewComment})");
        echo $db->getLink()->lastInsertId();
        $nestedSet->rebuild();
    } catch (PDOException $e) {
        echo "Database error: " . $e->getMessage();
    }
}
//Если пришел запрос Post с idComment и textComment, обновляем запись в таблице
if (isset($_POST['editComment'])) {
    try {
        $db->execute("UPDATE {$config['table_name']} SET text_comment = '{$_POST['txtComment']}' 
                                                        WHERE id = {$_POST['idComment']}");
        $nestedSet->rebuild();
        echo $_POST['idComment'];
    } catch (PDOException $e) {
        echo "Database error: " . $e->getMessage();
    }
}
//Если пришел запрос Post с textComment и isAnswer, создаем запись в таблице
if (isset($_POST['isAnswer'])) {
    $position = $nestedSet->getNewPosition($_POST['idAnswerComment']);
    try {
        $db->execute("INSERT INTO {$config['table_name']} (parent_id, text_comment, position) 
                          VALUES ('{$_POST['idAnswerComment']}', '{$_POST['txtComment']}', {$position})");
        echo $position;
        $nestedSet->rebuild();
    } catch (PDOException $e) {
        echo "Database error: " . $e->getMessage();
    }
}

$db->close();
