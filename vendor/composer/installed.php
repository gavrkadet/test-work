<?php return array(
    'root' => array(
        'name' => 'evgeny/test',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'evgeny/test' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'rundiz/nested-set' => array(
            'pretty_version' => 'dev-version1',
            'version' => 'dev-version1',
            'reference' => 'fce0b32516c1ae2e7bce5582cdf499c433c20402',
            'type' => 'library',
            'install_path' => __DIR__ . '/../rundiz/nested-set',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(
                0 => '1.27.x-dev',
            ),
            'dev_requirement' => true,
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => '6.3.x-dev',
            'version' => '6.3.9999999.9999999-dev',
            'reference' => 'eefb4e899f77fc9d7271072ab213d17d09bb4d52',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'dev_requirement' => true,
        ),
    ),
);
