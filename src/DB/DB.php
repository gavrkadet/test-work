<?php

declare(strict_types=1);

namespace Evgeny\Test\DB;

use PDO;
use PDOStatement;

class DB
{
    private PDO $link;

    protected bool|PDOStatement $statement;

    public function __construct()
    {
        $this->connect();
    }

    private function connect(): void
    {
        $config = require __DIR__ . '/../../config/config.php';
        $dsn = "mysql:dbname={$config['db_name']};host={$config['host']};port={$config['port']};charset={$config['charset']}";
        $this->link = new PDO($dsn,
            $config['username'],
            $config['password'],
            $config['options']);
    }

    public function execute($sql): bool
    {
        $sth = $this->link->prepare($sql);
        return $sth->execute();
    }

    public function getLink(): PDO
    {
        return  $this->link;
    }

    public function close()
    {
        unset($this->link);
    }
}
