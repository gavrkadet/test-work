<?php

declare(strict_types=1);

return [
    'host' => 'localhost',
    'port' => 3306,
    'db_name' => 'test',
    'table_name' => 'comments',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'UTF8',
    'options' => [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION // выбрасывает PDO Exception
    ]
];
